# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

user_input = int(input("How old are you? "))
user_input2 = (input("Do you have a signed consent form? y or n "))


def can_skydive(age, has_consent_form):

    if age > 18 or has_consent_form == "y":
        print('The person can skydive.')

    else:
        print('The person cannot sky dive.')


can_skydive(user_input, user_input2)
